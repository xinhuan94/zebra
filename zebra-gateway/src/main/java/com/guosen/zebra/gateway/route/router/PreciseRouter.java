package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.route.cache.ConcreteUrlRouteCache;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 精确匹配路由器
 */
@Component
public class PreciseRouter {
    private static final Logger LOGGER = LoggerFactory.getLogger(PreciseRouter.class);

    /**
     * 路由
     * @param requestURI    请求 URI
     * @return  路由信息
     */
    public RouteInfo route(String requestURI) {
        Map<String, RouteInfo> concreteUrlRouteInfoMap = ConcreteUrlRouteCache.getInstance().getUrlRouteInfoMap();
        RouteInfo preciseMatchRouteInfo = concreteUrlRouteInfoMap.get(requestURI);
        if (preciseMatchRouteInfo != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Route request URI [{}] to service [{}]", requestURI, preciseMatchRouteInfo);
            }
        }

        return preciseMatchRouteInfo;
    }
}
