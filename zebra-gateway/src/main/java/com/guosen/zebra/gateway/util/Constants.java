package com.guosen.zebra.gateway.util;

/**
 * 网关常量类
 */
public class Constants {

    /**
     * API网关URL前缀
     */
    public static final String GATE_WAY_URI_PREFIX = "/api";
}
