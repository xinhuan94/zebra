package com.guosen.zebra.gateway.route.parser;

import com.guosen.zebra.gateway.route.model.RouteConfig;
import com.guosen.zebra.gateway.route.model.RouteInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * URL前缀路由信息解析器
 */
public final class UrlPrefixRouteInfoParser {

    private UrlPrefixRouteInfoParser(){}

    /**
     * 解析
     * @param routeConfig   路由配置
     * @return  URL前缀到路由的映射
     */
    public static Map<String, RouteInfo> parse(RouteConfig routeConfig) {
        RouteInfo newRouteInfo = toRouteInfo(routeConfig);

        Map<String, RouteInfo> urlPrefixRouteInfo = new HashMap<>();
        urlPrefixRouteInfo.put(routeConfig.getUrlPrefix(), newRouteInfo);

        return urlPrefixRouteInfo;
    }

    private static RouteInfo toRouteInfo(RouteConfig routeConfig) {
        return RouteInfo.newBuilder()
                .serviceName(routeConfig.getServiceName())
                .version(routeConfig.getVersion())
                .group(routeConfig.getGroup())
                .urlPrefix(routeConfig.getUrlPrefix())
                .build();
    }
}
