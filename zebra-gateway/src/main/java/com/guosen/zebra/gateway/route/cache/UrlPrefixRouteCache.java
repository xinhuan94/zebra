package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * URL 前缀路由信息
 */
public final class UrlPrefixRouteCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(UrlPrefixRouteCache.class);

    /**
     * 缓存实例
     */
    private static final UrlPrefixRouteCache INSTANCE = new UrlPrefixRouteCache();
    
    /**
     * URL 前缀到路由信息的映射<br>
     *     key : URL 前缀; value : 路由信息
     */
    private Map<String, RouteInfo> urlPrefixRouteInfoMap = new HashMap<>();

    /**
     * 微服务包含的 URL 前缀
     */
    private Map<String, String> microServiceUrlPrefix = new HashMap<>();

    private UrlPrefixRouteCache(){}

    /**
     * 获取实例
     * @return  实例
     */
    public static UrlPrefixRouteCache getInstance() {
        return INSTANCE;
    }

    /**
     * 更新微服务的 URL 前缀路由信息
     * @param microServiceName  微服务名称
     * @param urlPrefixRouteInfoMapOfMicroservice   此微服务的 URL 前缀路由信息
     */
    public synchronized void update(String microServiceName, Map<String, RouteInfo> urlPrefixRouteInfoMapOfMicroservice) {
        LOGGER.info("Begin to update micro service {} url prefix route info cache", microServiceName);
        
        Map<String, RouteInfo> newUrlPrefixRouteInfoMap = new HashMap<>(urlPrefixRouteInfoMap);
        Map<String, String> newMicroServiceUrlPrefix = new HashMap<>(microServiceUrlPrefix);

        String oldUrlPrefix = microServiceUrlPrefix.get(microServiceName);
        if (oldUrlPrefix != null) {
            newMicroServiceUrlPrefix.remove(microServiceName);
            newUrlPrefixRouteInfoMap.remove(oldUrlPrefix);
        }

        if (!CollectionUtils.isEmpty(urlPrefixRouteInfoMapOfMicroservice)) {
            newUrlPrefixRouteInfoMap.putAll(urlPrefixRouteInfoMapOfMicroservice);

            // 虽然是map，但是其实只有一个
            newMicroServiceUrlPrefix.put(microServiceName, urlPrefixRouteInfoMapOfMicroservice.keySet().iterator().next());
        }

        urlPrefixRouteInfoMap = newUrlPrefixRouteInfoMap;
        microServiceUrlPrefix = newMicroServiceUrlPrefix;

        LOGGER.info("Finish to update micro service {} url prefix route info cache", microServiceName);
    }

    /**
     * 获取 URI 前缀路由信息映射
     * @return URI 前缀路由信息映射
     */
    public Map<String, RouteInfo> getUrlPrefixRouteInfo() {
        return urlPrefixRouteInfoMap;
    }
}
