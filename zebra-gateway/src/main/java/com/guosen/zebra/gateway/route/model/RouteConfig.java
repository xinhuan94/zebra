package com.guosen.zebra.gateway.route.model;

/**
 * 路由配置实体类
 */
public class RouteConfig {
    /**
     * 微服务全称
     */
    private String serviceName;

    /**
     * 版本
     */
    private String version;

    /**
     * 组
     */
    private String group;

    /**
     * 路径
     */
    private String urlPrefix;

    /**
     * 方法映射
     */
    private String methodMappings;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getMethodMappings() {
        return methodMappings;
    }

    public void setMethodMappings(String methodMappings) {
        this.methodMappings = methodMappings;
    }
}
