package com.guosen.zebra.gateway.route.parser;

import com.guosen.zebra.gateway.route.model.SubUrlMethodMapping;
import mockit.Tested;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.empty;

public class SubUrlMethodMappingParserTest {

    @Tested
    private SubUrlMethodMappingParser subUrlMethodMappingParser;

    @Test
    public void testParseBlankCfgStr() {
        List<SubUrlMethodMapping> result = SubUrlMethodMappingParser.parse("   ");

        assertThat(result, empty());
    }

    @Test
    public void testParse() {
        String cfgStr = "/hello->helloMethod\n/echo->echo\n/TRUMP->NYTimes->fakeNews";

        List<SubUrlMethodMapping> result = SubUrlMethodMappingParser.parse(cfgStr);

        assertThat(result.size(), is(2));

        SubUrlMethodMapping first = result.get(0);
        assertThat(first.getSubUrl(), is("/hello"));
        assertThat(first.getMethod(), is("helloMethod"));

        SubUrlMethodMapping second = result.get(1);
        assertThat(second.getSubUrl(), is("/echo"));
        assertThat(second.getMethod(), is("echo"));
    }

}
